Joyful Mysteries

(To be said on Monday and Saturday.)

1st joyful mystery - The Annunciation of the Angel to Mary

In the sixth month the angel Gabriel was sent from God unto a city of Galilee, named Nazareth, to a virgin espoused to a man whose name was Joseph, of the house of David; and the virgin's name was Mary. And the angel came in unto her, and said: "Hail, thou that art highly favoured, the Lord is with thee: blessed art thou among women". And when she saw Him, she was troubled at His saying, and cast in her mind what manner of salutation this should be.And the angel said unto her:" Fear not, Mary: for thou hast found favour with God. And, behold, thou shalt conceive in thy womb, and bring forth a son, and shalt call His name Jesus. He shall be great, and shall be called the Son of the Highest"... And Mary said:" Behold the handmaid of the Lord; be it unto me according to thy word". And the angel departed from her. (Luk 1,26-38)

Oh Mary with Your "yes" You have opened heaven's ways, You have achieved the will of the Father. You will be blessed for ever: Your intercession is heard in Heaven, because You have accepted God's plan. Let us pray that we can say "yes" to the Father in every instance He ask it of us. Mary help us to be humble and obedient to God's will.


2nd joyful mystery - The visitation of Mary to Saint Elizabeth

Mary arose in those days, and went into the hill country with haste, into a city of Juda. And entered into the house of Zacharias, and saluted Elisabeth. And it came to pass, that, when Elisabeth heard the salutation of Mary, the babe leaped in her womb; and Elisabeth was filled with the Holy Ghost and she spoke out with a loud voice, and said: "Blessed art thou among women, and blessed is the fruit of thy womb". ... And Mary said: "My soul doth magnify the Lord, and my spirit hath rejoiced in God my Saviour. For he hath regarded the low estate of His handmaiden: for, behold, from henceforth all generations shall call me blessed. For he that is mighty hath done to me great things; and holy is His name"...(Luk 1,39-49)

Mary goes to visit Elisabeth: She carries God to Her cousin, for the love of Her brothers and neighbours. Lord help us to carry Christ to others, like Mary did. Let us ask Mary the gift of burning charity.


3rd joyful mystery - The nativity of Jesus in Bethlehem

And so it was, that, while they were there, the days were accomplished that she should be delivered. And she brought forth her firstborn son, and wrapped Him in swaddling clothes, and laid Him in a manger; because there was no room for them in the inn. And there were in the same country shepherds abiding in the field, keeping watch over their flock by night. And, lo, the angel of the Lord came upon them, and the glory of the Lord shone round about them: and they were sore afraid. And the angel said unto them: "Fear not: for, behold, I bring you good tidings of great joy, which shall be to all people. For unto you is born this day in the city of David a Saviour, which is Christ the Lord. And this shall be a sign unto you; Ye shall find the babe wrapped in swaddling clothes, lying in a manger". (Luk 2,6-12)

Infant Jesus in the manger. Mary, Joseph and the shepherds adore Him. Let us also adore Christ, the Son of God, in the silence of our soul and from the depth of our heart. Let us ask Mary to make us love Jesus more, and also the gift of poverty of spirit.


4th joyful mystery - The presentation of Jesus to the Temple

And when the days of her purification according to the law of Moses were accomplished, they brought Him to Jerusalem, to present Him to the Lord; And to offer a sacrifice according to that which is said in the law of the Lord, A pair of turtledoves, or two young pigeons. And, behold, there was a man in Jerusalem, whose name was Simeon; and the same man was just and devout, waiting for the consolation of Israel: and the Holy Ghost was upon Him. ...When the parents brought in the child Jesus, to do for Him after the custom of the law, Then took he Him up in His arms, and blessed God, and said: "Lord, now lettest thou thy servant depart in peace, according to thy word: For mine eyes have seen thy salvation... A light to lighten the Gentiles, and the glory of thy people Israel." And Joseph and His mother marvelled at those things which were spoken of Him. And Simeon blessed them, and said unto Mary His mother: "Behold, this child is set for the fall and rising again of many in Israel; and for a sign which shall be spoken against; (Yea, a sword shall pierce through thy own soul also,) that the thoughts of many hearts may be revealed."(Luk 2,22-35)

We need to pay attention to the voice of God, to discern His call and accept the mission He gave us. After the Simeon prophecy, Mary takes into Her Heart the wound of sorrow, but in the silence She accepts the will of Her Father.

5th joyful mystery - The finding of Jesus in the Temple

Now His parents went to Jerusalem every year at the feast of the passover. And when he was twelve years old, they went up to Jerusalem after the custom of the feast. And when they had fulfilled the days, as they returned, the child Jesus tarried behind in Jerusalem; and Joseph and His mother knew not of it. But they, supposing Him to have been in the company, went a day's journey; and they sought Him among their kinsfolk and acquaintance. And when they found Him not, they turned back again to Jerusalem, seeking Him And it came to pass, that after three days they found Him in the temple, sitting in the midst of the doctors, both hearing them, and asking them questions. And all that heard Him were astonished at His understanding and answers. And when they saw Him, they were amazed: and His mother said unto Him: "Son, why hast thou thus dealt with us? behold, thy father and I have sought thee sorrowing". And he said unto them: "How is it that ye sought me? wist ye not that I must be about my Father's business?" And they understood not the saying which he spake unto them. And he went down with them, and came to Nazareth, and was subject unto them: but His mother kept all these sayings in her heart. (Luk 2,41-51)

Let us think of how many times we have been far from Jesus; from that Jesus, who with so much love has died for us. Let us meditate that in the difficulties of life the only safety is finding Jesus and never again leaving His great love.

